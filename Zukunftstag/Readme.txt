Preferences
1) File -> Preferences 
2) Set Sketchbook location: Path to the folder of Zukunftstag.ino file

Compiling Project:
1) Select Board: ESP32-C3 Dev Board
2) Start compiling (Arduino: Verify) -> Accept the requested installation messages
3) Select Board: ESP32C3 Dev Module

Upload Project
1) Download CP210x Driver from Silicon Labs Website
2) Unzip driver package
3) Install driver  (rightcklick on silabser.inf -> install)
4) In Arduino IDE select com port and upload sketch